
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class ApplicationProgram {

	public static void main (String args[]){
		//Get the instance of Customer Class with Springs
		 ApplicationContext context = new ClassPathXmlApplicationContext("Spring.xml");
		 Customer oCustomer = (Customer)context.getBean("customer");
		//Call quote premium calculator method
		 
		QuoteCalculator quoteCalculate = new QuoteCalculator();
		
		int premium = quoteCalculate.calculatePremium(oCustomer);
		System.out.println("Health Insurance Premium for "+oCustomer.getName()+" is "+premium);
		
	}
}
