
public class QuoteCalculator implements PremiumCalculator{
	private int basePremium = 5000;
	public int calculatePremium(Customer customer) {
		int totalPremium = basePremium;
		if(customer!=null){
			Health customerHealth = customer.getHealth();
			Habits customerHabits = customer.getHabits();
			int healthPercentage = validatePreHealthCheckUp(customerHealth);
			int habitsPercentage = validateHabits(customerHabits);
			int agePercentage = calculateAgePercentage(customer.getAge());
			int genderPercentage = validateGender(customer.getGender());
			totalPremium = totalPremium+(totalPremium*(healthPercentage+habitsPercentage+agePercentage+genderPercentage)/100);
			return totalPremium;
		}else {
			return 0;
		}
	}


	/**
	 * This method returns percentage of change after validating gender
	 * @param gender
	 * @return
	 */
	private int validateGender(String gender) {
		if("Male".equals(gender)){
			return 2;
		}else {
			return 0;
		}
	}


	/**
	 * This method returns percentage of change after validating age
	 * @param age
	 * @return
	 */
	private int calculateAgePercentage(int age) {
		//Percentage is considered as incremental & returned the value
		// Example if age is between 18 & 25 only 10 is incremented
		// else if it is between 25 to 30, 10 percentage increment should happen 2 times, hence the value 21 & so on
		if(age < 18){
			return 0;
		}else if(age>18 && age <25){
			return 10;
		}else if(age>25 && age <30){
			return 21;
		}else if(age>30 && age <35){
			return 33;
		}else {
			return 65;
		}
	}



	/**
	 * This method returns percentage of change after validating habits
	 * @param customerHabits
	 * @return
	 */
	private int validateHabits(Habits customerHabits) {
		int habitsPercentage = 0;
		if(customerHabits.isAlcohol()){
			habitsPercentage++;
		}
		if(customerHabits.isDailyExercise()){
			habitsPercentage--;
		}
		if(customerHabits.isDrugs()){
			habitsPercentage++;
		}
		if(customerHabits.isSmoking()){
			habitsPercentage++;
		}
		return habitsPercentage;
	}




	/**
	 * This method returns percentage of change after validating health checkup
	 * @param customerHealth
	 * @return
	 */
	private int validatePreHealthCheckUp(Health customerHealth) {
		int healthPercentage = 0;
		if(customerHealth.isBloodPressure()){
			healthPercentage++;
		}
		if(customerHealth.isBloodSugar()){
			healthPercentage++;
		}
		if(customerHealth.isHypertension()){
			healthPercentage++;
		}
		if(customerHealth.isOverWeight()){
			healthPercentage++;
		}
		return healthPercentage;
	}

}
